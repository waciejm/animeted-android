# AnimeTed

An android app to view images from [thisanimedoesnotexist.ai](https://thisanimedoesnotexist.ai) animated by the PSI value (also called creativity).

[Available on the Google Play Store](https://play.google.com/store/apps/details?id=me.mwojno.android.animeted)
