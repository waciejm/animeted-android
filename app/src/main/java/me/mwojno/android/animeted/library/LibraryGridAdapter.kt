package me.mwojno.android.animeted.library

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import me.mwojno.android.animeted.database.SavedAnimation
import me.mwojno.android.animeted.databinding.LibraryGridItemBinding

class LibraryGridAdapter(
    private val selectCallback: (String) -> Unit
) : ListAdapter<SavedAnimation, LibraryGridAdapter.SavedAnimationViewHolder>(DiffCallback) {

    class SavedAnimationViewHolder(
        private var binding: LibraryGridItemBinding,
        private var selectCallback: (String) -> Unit,
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(savedAnimation: SavedAnimation) {
            binding.savedAnimation = savedAnimation
            binding.root.setOnClickListener {
                selectCallback(savedAnimation.seed)
            }
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedAnimationViewHolder {
        return SavedAnimationViewHolder(
            LibraryGridItemBinding.inflate(LayoutInflater.from(parent.context)),
            selectCallback,
        )
    }

    override fun onBindViewHolder(holder: SavedAnimationViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object DiffCallback : DiffUtil.ItemCallback<SavedAnimation>() {

        override fun areItemsTheSame(oldItem: SavedAnimation, newItem: SavedAnimation): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: SavedAnimation, newItem: SavedAnimation): Boolean {
            return oldItem == newItem
        }
    }
}