package me.mwojno.android.animeted.animation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import me.mwojno.android.animeted.databinding.FragmentAnimationBinding

class AnimationFragment : Fragment() {

    private val viewModel: AnimationViewModel by viewModels()

    var playing = true
        set(value) {
            field = value
            if (field) {
                viewModel.resumeAnimation()
            }
        }

    var currentImage: Int
        get() = viewModel.visibleImage.value ?: 0
        set(index) = viewModel.setImage(index)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AnimationTimer(lifecycle) {
            if (playing) viewModel.nextAnimationImage()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentAnimationBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.animationViewModel = viewModel

        return binding.root
    }

    fun updateAnimationSeed(seed: String) = viewModel.updateAnimationSeed(seed)
}

const val ANIMATION_DELAY = 50L

class AnimationTimer(
    lifecycle: Lifecycle,
    private val animationCallback: () -> Unit
) : LifecycleObserver {

    private var handler = Handler(Looper.getMainLooper())
    private lateinit var runnable: Runnable

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startTimer() {
        runnable = Runnable {
            animationCallback()
            handler.postDelayed(runnable, ANIMATION_DELAY)
        }
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed(runnable, ANIMATION_DELAY)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopTimer() {
        handler.removeCallbacks(runnable)
    }
}