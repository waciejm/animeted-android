package me.mwojno.android.animeted.explore

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import me.mwojno.android.animeted.database.SavedAnimationDatabaseDao


class ExploreViewModelFactory(
    private val dataSource: SavedAnimationDatabaseDao,
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExploreViewModel::class.java)) {
            return ExploreViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}