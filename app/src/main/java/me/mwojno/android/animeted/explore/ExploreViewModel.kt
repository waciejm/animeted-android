package me.mwojno.android.animeted.explore

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import me.mwojno.android.animeted.database.SavedAnimation
import me.mwojno.android.animeted.database.SavedAnimationDatabaseDao
import kotlin.random.Random
import kotlin.random.nextInt

const val KEY_BUNDLE_SEED = "explore_seed_key"

class ExploreViewModel(dataSource: SavedAnimationDatabaseDao) : ViewModel() {

    private val savedAnimationDao = dataSource

    private val _seed = MutableLiveData<String>()
    val seed: LiveData<String>
        get() = _seed

    val seedText: LiveData<String> = Transformations.map(seed) {
        "Seed: $it"
    }

    private val _saved = MutableLiveData<SavedAnimation>()
    val saved: LiveData<SavedAnimation?>
        get() = _saved

    private val _autoAnimated = MutableLiveData(true)
    val autoAnimated: LiveData<Boolean>
        get() = _autoAnimated

    fun randomAnimationSeed() {
        trySetAnimationSeed(genRandomSeed())
    }

    fun trySetAnimationSeed(seed: String): Boolean {
        return if (
            seed.length != 5
            || seed.find { !it.isDigit() } != null
        ) {
            false
        } else {
            updateSeed(seed)
            true
        }
    }

    fun onToggleSaved() {
        viewModelScope.launch {
            val savedAnimation = saved.value
            val animationSeed = seed.value
            if (savedAnimation != null) {
                savedAnimationDao.delete(savedAnimation)
                _saved.value = null
            } else if (animationSeed != null) {
                savedAnimationDao.insert(SavedAnimation(animationSeed))
                _saved.value = savedAnimationDao.get(animationSeed)
            }
        }
    }

    fun onToggleAutoAnimated() {
        _autoAnimated.value = (_autoAnimated.value ?: false).not()
    }

    private fun updateSeed(seed: String) {
        _seed.value = seed
        viewModelScope.launch {
            _saved.value = savedAnimationDao.get(seed)
        }
    }

    private fun genRandomSeed(): String {
        return "%05d".format(Random.nextInt(0..99999))
    }

    fun saveBundle(bundle: Bundle) {
        bundle.putString(KEY_BUNDLE_SEED, seed.value)
    }

    fun loadBundle(bundle: Bundle?) {
        if (bundle != null) {
            val savedSeed = bundle.getString(KEY_BUNDLE_SEED)
            if (savedSeed != null) {
                _seed.value = savedSeed
            } else {
//                Log.w("ExploreViewModel", "KEY_BUNDLE_SEED not in loaded bundle")
                randomAnimationSeed()
            }
        } else {
            randomAnimationSeed()
        }
    }
}