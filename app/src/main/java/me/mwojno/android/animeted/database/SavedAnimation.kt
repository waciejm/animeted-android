package me.mwojno.android.animeted.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Entity(tableName = "saved_animation_table")
data class SavedAnimation(
    @PrimaryKey
    val seed: String,
    @ColumnInfo(name = "time_saved")
    val timeSaved: Long = System.currentTimeMillis()
)

@Dao
interface SavedAnimationDatabaseDao {

    @Insert
    suspend fun insert(animation: SavedAnimation)

    @Delete
    suspend fun delete(animation: SavedAnimation)

    @Query("SELECT * FROM saved_animation_table WHERE seed = :seed")
    suspend fun get(seed: String): SavedAnimation?

    @Query("SELECT * FROM saved_animation_table ORDER BY time_saved DESC")
    fun getSavedAnimations(): LiveData<List<SavedAnimation>>
}

@Database(entities = [SavedAnimation::class], version = 1, exportSchema = false)
abstract class SavedAnimationDatabase : RoomDatabase() {

    abstract val savedAnimationsDatabaseDao: SavedAnimationDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: SavedAnimationDatabase? = null

        fun getInstance(context: Context): SavedAnimationDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        SavedAnimationDatabase::class.java,
                        "saved_animation_database"
                    ).fallbackToDestructiveMigration().build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}