package me.mwojno.android.animeted.library

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import me.mwojno.android.animeted.database.SavedAnimationDatabaseDao


class LibraryViewModelFactory(
    private val dataSource: SavedAnimationDatabaseDao,
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LibraryViewModel::class.java)) {
            return LibraryViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}