package me.mwojno.android.animeted.library

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import me.mwojno.android.animeted.R
import me.mwojno.android.animeted.database.SavedAnimation

@BindingAdapter("libraryThumbnailUrl")
fun ImageView.libraryThumbnailUrl(seed: String?) {
    if (seed != null) {
        val url = "https://thisanimedoesnotexist.ai/results/psi-0.5/seed${seed}.png"
        val uri = url.toUri().buildUpon().scheme("https").build()
        Glide.with(context)
            .load(uri)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .apply(
                RequestOptions()
                    .error(R.drawable.ic_connection_error)
                    .placeholder(R.drawable.loading_animation)
            )
            .into(this)
    }
}

@BindingAdapter("librarySavedAnimations")
fun RecyclerView.librarySavedAnimations(data: List<SavedAnimation>?) {
    val adapter = adapter as LibraryGridAdapter
    adapter.submitList(data)
}

