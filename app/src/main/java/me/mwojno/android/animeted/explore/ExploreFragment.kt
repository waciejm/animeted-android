package me.mwojno.android.animeted.explore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.app.AlertDialog
import android.content.Intent
import android.text.InputType
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.google.android.material.slider.Slider
import me.mwojno.android.animeted.R
import me.mwojno.android.animeted.animation.AnimationFragment
import me.mwojno.android.animeted.database.SavedAnimationDatabase
import me.mwojno.android.animeted.databinding.FragmentExploreBinding

class ExploreFragment : Fragment() {

    private val viewModel by viewModels<ExploreViewModel> {
        val database = SavedAnimationDatabase.getInstance(requireContext())
        ExploreViewModelFactory(database.savedAnimationsDatabaseDao)
    }

    private val args by navArgs<ExploreFragmentArgs>()

    private lateinit var animationFragment: AnimationFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.loadBundle(savedInstanceState)

        val navController = findNavController()
        (activity)?.let {
            NavigationUI.setupActionBarWithNavController(it as AppCompatActivity, navController)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentExploreBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.exploreViewModel = viewModel
        binding.exploreSelectButton.setOnClickListener {
            onSelectSeedClicked()
        }
        binding.exploreLibraryButton.setOnClickListener {
            findNavController()
                .navigate(ExploreFragmentDirections.actionExploreFragmentToLibraryFragment())
        }
        binding.exploreAnimateButton.setOnClickListener {
            viewModel.onToggleAutoAnimated()
            if (viewModel.autoAnimated.value == false) {
                binding.exploreImageSlider.value = (animationFragment.currentImage + 1).toFloat()
            }
        }
        binding.exploreImageSlider.addOnChangeListener(object : Slider.OnChangeListener {
            override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) {
                val index = value.toInt() - 1
                animationFragment.currentImage = index
            }

        })

        animationFragment =
            childFragmentManager.findFragmentById(R.id.explore_animation_fragment) as AnimationFragment

        viewModel.seed.observe(viewLifecycleOwner, { seed ->
            seed?.let {
                animationFragment.updateAnimationSeed(it)
            }
        })

        viewModel.autoAnimated.observe(viewLifecycleOwner, { playing ->
            playing?.let {
                animationFragment.playing = it
            }
        })

        if (args.seed != "") {
            viewModel.trySetAnimationSeed(args.seed)
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_explore, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.about_fragment ->
                NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                    || super.onOptionsItemSelected(item)
            R.id.share_gif -> {
                onShareAnimation()
                super.onOptionsItemSelected(item)
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.saveBundle(outState)
    }

    private fun onSelectSeedClicked() {
        val context = requireNotNull(this.activity)

        val editText = EditText(context)
        editText.inputType = InputType.TYPE_CLASS_NUMBER

        val dialog = AlertDialog.Builder(context)
            .setTitle(getString(R.string.select_seed))
            .setMessage(getString(R.string.enter_new_seed))
            .setView(editText)
            .setPositiveButton(getString(R.string.select)
            ) { _, _ -> viewModel.trySetAnimationSeed(editText.text.toString()) }
            .setNegativeButton(getString(R.string.cancel), null)
            .create()
        dialog.show()
    }

    private fun onShareAnimation() {
        val seed = viewModel.seed.value
        if (seed != null) {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(
                Intent.EXTRA_TEXT,
                "https://waciejm.gitlab.io/animeted-web-share/?seed=$seed"
            )
            startActivity(Intent.createChooser(intent, "Share"))
        }
    }
}