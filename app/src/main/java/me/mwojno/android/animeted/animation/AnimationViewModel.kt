package me.mwojno.android.animeted.animation

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

const val IMAGES = 18
const val ANIMATION_FRAMES_FREEZE = 2
const val ANIMATION_FRAMES = (IMAGES + ANIMATION_FRAMES_FREEZE) * 2

class AnimationViewModel : ViewModel() {
    // Loader used to load animation images and synchronize when all are loaded
    private val _animationLoader = MutableLiveData<AnimationImagesLoader>()
    val animationLoader: LiveData<AnimationImagesLoader>
        get() = _animationLoader

    // Handles to pass to
    private val _animationLoaderHandles = MutableLiveData<ArrayList<AnimationImagesLoader.Handle>>()
    val animationLoaderHandles: LiveData<ArrayList<AnimationImagesLoader.Handle>>
        get() = _animationLoaderHandles

    // Currently visible image
    private val _visibleImage = MutableLiveData<Int>()
    val visibleImage: LiveData<Int>
        get() = _visibleImage

    // Currently frame of the animation
    private var currentFrame = 0

    // Change the shown animation to one with the provided seed
    fun updateAnimationSeed(seed: String) {
        currentFrame = 0

        _animationLoader.value = AnimationImagesLoader(seed, _animationLoader)
        _animationLoaderHandles.value = animationLoader.value?.getHandles()
    }

    // Show the next frame of the animation
    fun nextAnimationImage() {
        val loader = animationLoader.value
        if (loader != null && loader.status == AnimationImagesLoader.Status.LOADED) {
            currentFrame = (currentFrame + 1) % ANIMATION_FRAMES
            updateVisibleImage()
        }
    }

    // Set visible image manually
    fun setImage(index: Int) {
        if (index in 0 until IMAGES) {
            _visibleImage.value = index
        }
    }

    // Resume the animation from the currently visible image
    fun resumeAnimation() {
        currentFrame = ((visibleImage.value ?: 0) + ANIMATION_FRAMES_FREEZE).coerceIn(0 until IMAGES)
        updateVisibleImage()
    }

    // Update which image is currently shown
    private fun updateVisibleImage() {
        _visibleImage.value = when {
            currentFrame < ANIMATION_FRAMES_FREEZE -> {
                0
            }
            currentFrame < ANIMATION_FRAMES / 2 -> {
                currentFrame - ANIMATION_FRAMES_FREEZE
            }
            currentFrame < ANIMATION_FRAMES / 2 + ANIMATION_FRAMES_FREEZE -> {
                IMAGES - 1
            }
            else -> {
                ANIMATION_FRAMES - currentFrame - 1
            }
        }
    }
}

class AnimationImagesLoader(
    // Seed of the loaded animation
    private val seed: String,
    // MutableLiveData reference which holds the loader, for signalling changes
    private val liveData: MutableLiveData<AnimationImagesLoader>,
) {
    // Status of the images
    enum class Status { LOADING, LOADED, ERROR }
    val status: Status
        get() = when {
            error -> Status.ERROR
            loadedImages == IMAGES -> Status.LOADED
            else -> Status.LOADING
        }

    val isLoading: Boolean
        get() = status == Status.LOADING

    val isLoaded: Boolean
        get() = status == Status.LOADED

    val isError: Boolean
        get() = status == Status.ERROR

    // Handles taken by workers which load images
    fun getHandles(): ArrayList<Handle> {
        return (0 until IMAGES).map {
            Handle(this, imageUri(seed, it))
        }.toCollection(arrayListOf())
    }

    class Handle(
        private val loader: AnimationImagesLoader,
        val uri: Uri,
    ) {
        // Called by the worker after finishing to load the image
        fun onImageLoaded() {
            loader.onImageLoaded()
        }

        // Called by the worker if loading the image failed
        fun onImageError() {
            loader.onImageError()
        }
    }

    private var loadedImages = 0
    private var error = false

    private fun onImageLoaded() {
        synchronized(this) {
            val oldStatus = status
            loadedImages += 1
            if (status != oldStatus) {
                liveData.value = liveData.value
            }
        }
    }

    private fun onImageError() {
        synchronized(this) {
            val oldStatus = status
            error = true
            if (status != oldStatus) {
                liveData.value = liveData.value
            }
        }
    }

    private fun imageUri(seed: String, index: Int): Uri {
        val psi = ((3 + index) / 10).toString() + "." + ((3 + index) % 10).toString()
        val url = "https://thisanimedoesnotexist.ai/results/psi-${psi}/seed${seed}.png"
        return Uri.parse(url)
    }
}
