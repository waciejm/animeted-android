package me.mwojno.android.animeted.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.mwojno.android.animeted.R
import me.mwojno.android.animeted.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentAboutBinding.inflate(inflater)

        binding.aboutTadneLink.setOnClickListener {
            val url = getString(R.string.tadne_url)
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = uri
            startActivity(intent)
        }

        return binding.root
    }
}