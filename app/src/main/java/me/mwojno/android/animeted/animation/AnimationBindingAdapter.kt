package me.mwojno.android.animeted.animation

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import me.mwojno.android.animeted.R

@BindingAdapter("animationImageUrl")
fun ImageView.animationImageUrl(handle: AnimationImagesLoader.Handle?) {
    if (handle != null) {
        Glide.with(context)
            .load(handle.uri)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .apply(
                RequestOptions()
                    .error(R.drawable.ic_connection_error)
            )
            .listener(object : RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    handle.onImageLoaded()
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    handle.onImageError()
                    return false
                }
            })
            .into(this)
    }
}
