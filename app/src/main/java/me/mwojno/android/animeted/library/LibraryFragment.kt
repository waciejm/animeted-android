package me.mwojno.android.animeted.library

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import me.mwojno.android.animeted.R
import me.mwojno.android.animeted.database.SavedAnimationDatabase
import me.mwojno.android.animeted.databinding.FragmentLibraryBinding

class LibraryFragment : Fragment() {

    private val viewModel: LibraryViewModel by viewModels {
        val database = SavedAnimationDatabase.getInstance(requireContext())
        LibraryViewModelFactory(database.savedAnimationsDatabaseDao)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentLibraryBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.libraryViewModel = viewModel

        binding.libraryGrid.adapter = LibraryGridAdapter {
            onSelectSavedAnimation(it)
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_library, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }

    private fun onSelectSavedAnimation(seed: String) {
        val nav = LibraryFragmentDirections.actionLibraryFragmentToExploreFragment()
        nav.seed = seed
        findNavController().navigate(nav)
    }
}