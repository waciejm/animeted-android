package me.mwojno.android.animeted.library

import androidx.lifecycle.ViewModel
import me.mwojno.android.animeted.database.SavedAnimationDatabaseDao

class LibraryViewModel(dataSource: SavedAnimationDatabaseDao) : ViewModel() {

    val savedAnimations = dataSource.getSavedAnimations()
}